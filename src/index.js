const bodyParser = require('body-parser');
const mongoose = require('mongoose');

// function testDependencies() {
//   console.log("testing dependency exists: mongoose")
//   var mongoose = require('mongoose');
// }

function registerApp(app, config) {
  console.log('registering app from hosted function');
  mongoose.connect(config.mongodbUri, {useNewUrlParser: true});
  app.use(bodyParser.json());
  app.post('/echo', (req, res) => {
    res.json(req.body);
  });
  app.get('/dbstats', (req, res) => {
    mongoose.connection.db.stats().then((info) => {
      res.json(info)
    }).catch((err) => {
      res.status(500).json({
        error_code: 'INTERNAL_SERVER_ERROR',
        error_message: err.message,
      })
    })
  })
  return app;
}

module.exports = registerApp;
